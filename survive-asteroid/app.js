let speed = 10;
let spin  = 0;
let iniX = 10;
let cantObstacles = 2;
let score = 0;
let firstColor = '#999'
let movColor   = '#0b6'
let timesLap   = 0;
let recomp     = 1000;
let dificulty  = 50;
let heightArea = 500;
let sprite     = 'assets/laser-03.png'
let asteroid   = 'assets/asteroid.png'

// START CANVAS AREA
document.querySelectorAll("canvas")[0].width   = window.innerWidth;
document.querySelectorAll("canvas")[0].height  = heightArea;
document.getElementById('restartBtn').disabled = true;

window.restartGame = function (){
  timesLap = 0;
  score    = 0;
  gameEngine()
}

let gameEngine = function () {
  
  window.updateState = function (){

    if(timesLap == 1){
      score = score + recomp;
      timesLap = 0;

    } else {
      score++;
    }

    let ctx = myGameArea.canvas.getContext("2d");
    
    let inputText = function (){
      ctx.font      = "1rem Arial";
      ctx.fillStyle = '#fff';
      ctx.fillText("SCORE: " + score, 10, 30);
    }

    if (myGamePiece.crashWith(myObstacle)) {
      myGameArea.stop();
    } else {
      myGameArea.clear();
      myGamePiece.newPos();
      myGamePiece.update();
      myObstacle.update();
      inputText();

      // SET NEW SEED
      myObstacle = new staticComponent(30, Math.random() * (myGameArea.width - 10), firstColor, Math.random() * (myGameArea.width - 30), Math.random() * (myGameArea.height - 10) );
    }
  }

  myGameArea.start()

  let myGamePiece = new movilComponent(30, 30, movColor, iniX, 120);

  // GENERATE RAND OBSTACLES
  let myObstacle  = new staticComponent(30, Math.random() * (myGameArea.width - 10), firstColor, Math.random() * (myGameArea.width - 30), Math.random() * (myGameArea.height - 10) );

  let arrowMoves = {
    ArrowUp: function (){
      myGamePiece.speedY = spin;
      myGamePiece.speedY -= speed
      myGamePiece.speedX = spin;
    },
    ArrowDown: function (){
      myGamePiece.speedY = spin;
      myGamePiece.speedY += speed
      myGamePiece.speedX = spin;
    }, 
    ArrowLeft: function (){
      myGamePiece.speedX = spin;
      myGamePiece.speedX -= speed
      myGamePiece.speedY = spin
    },
    ArrowRight: function (){
      myGamePiece.speedX = spin;
      myGamePiece.speedX += speed
      myGamePiece.speedY = spin
    }
  }

  // MOVMENT KEY PRESS
  window.addEventListener('keydown', function (e){
    try {
      arrowMoves[e.key]()
    } catch (e){}
  })
}

let staticComponent = function(width, height, color, x, y) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y;
  this.image = new Image();
  this.image.src = sprite;

  ctx = myGameArea.context;
  // ctx.fillStyle = color;
  // ctx.fillRect(this.x, this.y, this.width, this.height);

  ctx.drawImage(this.image,
        this.x,
        this.y,
        this.width, this.height);
  
  this.update = function() {
    ctx = myGameArea.context;
    // ctx.fillStyle = color;
    // ctx.fillRect(this.x, this.y, this.width, this.height);

    ctx.drawImage(this.image,
        this.x,
        this.y,
        this.width, this.height);
  }
}

let movilComponent = function(width, height, color, x, y) {
  this.width = width;
  this.height = height;
  this.x = x;
  this.y = y

  // ADD IMAGES
  this.image = new Image();
  this.image.src = asteroid;

  // AGREGAR VELOCIDADES
  this.speedX = 0;
  this.speedY = 0;
  ctx = myGameArea.context;
  // ctx.fillStyle = color;
  // ctx.fillRect(this.x, this.y, this.width, this.height);

  ctx.drawImage(this.image,
        this.x,
        this.y,
        this.width, this.height);

  this.newPos = function() {

    // MIDO LA DISTANCIA
    let dXmin = this.x + this.width;
    let dXmax = myGameArea.width - this.width;


    let dYmin = this.y + this.height;
    let dYmax = myGameArea.height - this.height;

    // BUSCO EL MINIMO DEL SPEED
    if(this.speedX < 0 && Math.abs(this.speedX) >= dXmin){
      this.x = 0;
      timesLap = 0.5;

      return;
    }

    // BUSCO EL MAXIMO DE IZQ A DERECHA
    if(this.speedX > 0 && ((this.x + this.speedX) > dXmax)){
      this.x = dXmax;
      timesLap = timesLap * 2;
      return;
    }

    if(this.speedY < 0 && Math.abs(this.speedY) >= dYmin){
      this.y = 0;
      return;
    }

    if(this.speedY > 0 && ((this.y + this.speedY) > dYmax)){
      this.y = dYmax;
      return;
    }

    this.x += this.speedX;
    this.y += this.speedY;

  }
  // METODO PARA COLICION
  this.crashWith = function (objColider){
    let otherobj  = objColider;
    let crash     = true;

    // objColider.map( function (otherobj) {
      let myleft    = this.x;
      let myright   = this.x + (this.width);
      let mytop     = this.y;
      let mybottom  = this.y + (this.height);
      let otherleft = otherobj.x;
      let otherright= otherobj.x + (otherobj.width);
      let othertop  = otherobj.y;
      let otherbottom = otherobj.y + (otherobj.height);
      
      let colision = (mybottom < othertop) || (mytop > otherbottom) || (myright < otherleft) || (myleft > otherright);

      if (colision) {
        crash = false;
      }
    return crash;
  }
  this.update = function() {
    ctx = myGameArea.context;
    // ctx.fillStyle = color;
    // ctx.fillRect(this.x, this.y, this.width, this.height);

    ctx.drawImage(this.image,
        this.x,
        this.y,
        this.width, this.height);
  }
}

let myGameArea = {
  canvas: document.querySelectorAll("canvas")[0],
  start: function() {
    document.getElementById('restartBtn').disabled = true;
    this.canvas.width = window.innerWidth;
    this.canvas.height = heightArea;
    this.context = this.canvas.getContext("2d");

    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
    this.interval = setInterval(updateState, dificulty);
  },
  width: window.innerWidth,
  height: heightArea,
  clear : function(){
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  },
  stop: function (){
    clearInterval(this.interval);
    document.getElementById('restartBtn').disabled = false;
  }
}


let startTheGame = function () {
  document.getElementById('startBtn').disabled = true;

  // RUN ENGINE
  gameEngine()
}