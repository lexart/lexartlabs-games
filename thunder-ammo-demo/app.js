let config = {
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 300 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

let game = new Phaser.Game(config);
let platforms;
let player;
let cursors;
let score = 0;
let scoreText;
let stars;
let bombs;
let gameOver = false;

function preload (){
    this.load.image('sky', 'assets/pixil-frame-5.png');
    this.load.image('ground', 'assets/ground-01.png');
    this.load.image('star', 'assets/food-001.png');
    this.load.image('bomb', 'assets/asteroid-001.png');
    this.load.spritesheet('dude', 
        'assets/dude-sprite-01.png',
        { frameWidth: 50, frameHeight: 116 }
    );
    console.log("load: ", this.load);
}

function create (){
    this.add.image(0, 0, 'sky').setOrigin(0, 0).setScale(2);
    this.add.image(980, 600, 'sky').setScale(2);

    platforms = this.physics.add.staticGroup();
    
    platforms.create(0, 568, 'ground').setScale(2).refreshBody();
    platforms.create(400, 568, 'ground').setScale(2).refreshBody();
    platforms.create(800, 568, 'ground').setScale(2).refreshBody();
    platforms.create(1200, 568, 'ground').setScale(2).refreshBody();
    
    // GROUND OBSTACLES
    platforms.create(600, 380, 'ground');
    platforms.create(100, 350, 'ground');
    platforms.create(750, 300, 'ground');
    platforms.create(1000, 300, 'ground');
    platforms.create(1200, 200, 'ground');

    // PLAYER
    player = this.physics.add.sprite(100, 450, 'dude');

    player.setBounce(0.2);
    player.setCollideWorldBounds(true);

    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('dude', { start: 0, end: 2 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'turn',
        frames: [ { key: 'dude', frame: 4 } ],
        frameRate: 20
    });

    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('dude', { start: 4, end: 8 }),
        frameRate: 10,
        repeat: -1
    });

    bombs     = this.physics.add.group();
    scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#fff' });

    stars = this.physics.add.group({
        key: 'star',
        repeat: 11,
        setXY: { x: 12, y: 0, stepX: 70 }
    });

    stars.children.iterate(function (child) {
        child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8));
    });

    let x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

    let bomb = bombs.create(x, 16, 'bomb').setScale(3);
    bomb.setBounce(1);
    bomb.setCollideWorldBounds(true);
    bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
    bomb.allowGravity = false;

    this.physics.add.collider(player, platforms);
    this.physics.add.collider(stars, platforms);
    this.physics.add.collider(bombs, platforms);

    this.physics.add.overlap(player, stars, collectStar, null, this);
    this.physics.add.collider(player, bombs, hitBomb, null, this);
}

function update () {
    cursors = this.input.keyboard.createCursorKeys();
    // SETEA LA GRAVEDAD
    player.body.setGravityY(300)

    // console.log("player.body.touching.down: ", player.body.touching.down)

    // INTERSECCION DEL JUGADOR CON EL MAPA
    this.physics.add.collider(player, platforms);

    if (gameOver){
        return;
    }

    if (cursors.left.isDown){
        player.setVelocityX(-260);

        player.anims.play('left', true);
    }
    else if (cursors.right.isDown){
        player.setVelocityX(260);

        player.anims.play('right', true);
    }
    else{
        player.setVelocityX(0);

        player.anims.play('turn');
    }

    if (cursors.up.isDown && player.body.touching.down){
        
        player.setVelocityY(-550);
    }
}

let asteroidSize = 2;
function collectStar (player, star){
    star.disableBody(true, true);

    score += 10;
    scoreText.setText('Score: ' + score);
    console.log("stars.countActive(true): ", stars.countActive(true));

    if (stars.countActive(true) < 5){

        //  A new batch of stars to collect
        stars.children.iterate(function (child) {

            child.enableBody(true, child.x, 0, true, true);

        });



        let x = (player.x < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);

        let bomb = bombs.create(x, 16, 'bomb').setScale(asteroidSize);
        bomb.setBounce(1);
        bomb.setCollideWorldBounds(true);
        bomb.setVelocity(Phaser.Math.Between(-200, 200), 20);
        bomb.allowGravity = false;

        asteroidSize++;

    }
}
function hitBomb (player, bomb){
    this.physics.pause();

    player.setTint(0xff0000);
    bomb.setTint(0xff0000);

    player.anims.play('turn');

    gameOver = true;
}